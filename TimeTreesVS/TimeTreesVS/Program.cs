﻿using System;
using System.Collections.Generic;
using TimeTreesVS.Models;
using Newtonsoft.Json;
using System.IO;

namespace TimeTreesVS
{
    class Program
    {             
        static void Main(string[] args)
        {
            Console.WriteLine("Введите разрешение файлов '.csv' или '.json' на выбор:");
            string extension = Console.ReadLine();

            if (extension == ".csv"|| extension == ".json")
            {
                Files.CheckFiles(extension);
                People[] peoples = null;
                Event[] events = null;
                if (extension == ".json")
                {
                    peoples = JsonConvert.DeserializeObject<People[]>(File.ReadAllText(Environment.CurrentDirectory + "/" + Files.NameOfFilePeople + extension));
                    events = JsonConvert.DeserializeObject<Event[]>(File.ReadAllText(Environment.CurrentDirectory + "/" + Files.NameOfFileTime + extension));
                }
                else
                {
                    peoples = Files.ReadPeople(Environment.CurrentDirectory + "/" + Files.NameOfFilePeople + extension);
                    events = Files.ReadEvent(Environment.CurrentDirectory + "/" + Files.NameOfFileTime + extension);
                }

                (int years, int months, int days) = Utils.DeltaMinAndMaxDate(events);
                Console.WriteLine($"Между макс и мин датами прошло: {years} лет, {months} месяцев и {days} дней");
                foreach (var item in DetectNames(peoples))
                {
                    if (item != null)
                    {
                        Console.WriteLine("\t" + item);
                    }

                }
            }
            else
            {
                Console.WriteLine("Неправельный формат!");
            }
                     
        }
        static string[] DetectNames(People[] peoples)
        {
            string[] names = new string[peoples.Length];
            for (int i = 0; i < peoples.Length; i++)
            {
                People item = peoples[i];
                if (!DateTime.IsLeapYear(item.Birthday.Year))
                {
                    continue;
                }
                TimeSpan lifeTime = item.DeathDay.Subtract(item.Birthday);
                int age = (DateTime.MinValue + lifeTime).Year - 1;
                if (age > 20)
                {
                    continue;
                }
                names[i] = item.Name;
            }
            return names;
        }
                      
    }
}
