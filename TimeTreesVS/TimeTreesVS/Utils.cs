﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TimeTreesVS.Models;

namespace TimeTreesVS
{
    public struct Utils
    {
        public static DateTime ParseDate(string value) //******
        {
            DateTime date;
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        throw new Exception("WRONG FORMAT");
                    }
                }
            }

            return date;
        }
        public static (int, int, int) DeltaMinAndMaxDate(Event[] timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            TimeSpan differenceDate = maxDate.Subtract(minDate);
            DateTime resultDate = DateTime.MinValue + differenceDate;
            return (resultDate.Year - 1,
                resultDate.Month - 1,
                resultDate.Day - 1);
        }
        private static (DateTime, DateTime) GetMinAndMaxDate(Event[] timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (var timeEvent in timeline)
            {
                DateTime date = timeEvent.Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }

            return (minDate, maxDate);
        }
    }
}
