﻿using System;

namespace TimeTreesVS.Models
{
    public struct People
    {

        public int Id;

        public string Name;
        public DateTime Birthday;
        public DateTime DeathDay; 



        public People(string[] values)
        {
            Id = Convert.ToInt32(values[0]);
            Name = values[1];
            Birthday = Utils.ParseDate(values[2]);

            if (values.Length == 4)
            {
                DeathDay = Utils.ParseDate(values[3]);
            }
            else
            {
                DeathDay = DateTime.Now;
            }


        }
    }
}
