﻿using System;

namespace TimeTreesVS.Models
{
    public struct Event
    {
        public DateTime Date;

        public string Content;

       public  Event(string[] values) 
        {
            Date = Utils.ParseDate(values[0]);
            Content = values[1];
        }
    }
}
