﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TimeTreesVS.Models;

namespace TimeTreesVS
{
    public struct Files
    {
        public static string NameOfFilePeople = "people";
        public static string NameOfFileTime = "timeline";
        public static void WriteTestFiles(string pathToFile)
        {
            if (Path.GetFileName(pathToFile) == NameOfFilePeople + ".csv")
            {
                File.WriteAllLines(pathToFile, GeneratePeopleData());
            }
            if (Path.GetFileName(pathToFile) == NameOfFileTime + ".csv")
            {
                File.WriteAllLines(pathToFile, GenerateTimelineData());
            }
            if (Path.GetFileName(pathToFile) == NameOfFilePeople + ".json")
            {
                if (!File.Exists(Environment.CurrentDirectory + "/" + NameOfFilePeople + ".csv"))
                {
                    File.WriteAllLines(Environment.CurrentDirectory + "/" + NameOfFilePeople + ".csv", GeneratePeopleData());
                }
                People[] peoples = ReadPeople(NameOfFilePeople + ".csv");

                string jsonPeople = JsonConvert.SerializeObject(peoples);
                File.WriteAllText(Environment.CurrentDirectory + "/" + NameOfFilePeople + ".json", jsonPeople);

                
            }
            if (Path.GetFileName(pathToFile) == NameOfFileTime + ".json")
            {
                if (!File.Exists(Environment.CurrentDirectory + "/" + NameOfFileTime + ".csv"))
                {
                    File.WriteAllLines(Environment.CurrentDirectory + "/" + NameOfFileTime + ".csv", GenerateTimelineData());
                }
                Event[] events = ReadEvent(NameOfFileTime + ".csv");
                string jsonEvent = JsonConvert.SerializeObject(events);
                File.WriteAllText(Environment.CurrentDirectory + "/" + NameOfFileTime + ".json", jsonEvent);
            }
        }
        private static string[] GenerateTimelineData()
        {
            return new[]
            {
                "1950;событие 1 бла-бла-бла",
                "1991-06-01;какое-то событие 2",
                "2000-01-01;наступил миллениум, ура-ура-ура"
            };
        }

        private static string[] GeneratePeopleData()
        {
            return new[]
            {
                "1;Имя 1;2000-06-05;2019-05-01",
                "2;Имя 2;1950-01-10;2010-01-01",
                "3;Имя 3;2003-01-10;2010-01-01",
                "4;Имя 4;2004-01-10;2010-01-01",
                "5;Имя 5;1980-01-10"
            };
        }
        public static void CheckFiles(string extension)
        {
            string pathToTimelineFile = Path.Combine(Environment.CurrentDirectory, NameOfFileTime + extension);
            string pathToPeopleFile = Path.Combine(Environment.CurrentDirectory, NameOfFilePeople + extension);

            if (!File.Exists(pathToTimelineFile))
            {
                WriteTestFiles(pathToTimelineFile);
            }
            if (!File.Exists(pathToPeopleFile))
            {
                WriteTestFiles(pathToPeopleFile);
            }
        }

        public static People[] ReadPeople(string path)
        {
            string[] data = File.ReadAllLines(path);
            People[] array = new People[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                string line = data[i];
                string[] parts = line.Split(";");

                People item = new People(parts);
                array[i] = item;
            }

            return array;
        }
        public static Event[] ReadEvent(string path)
        {
            string[] data = File.ReadAllLines(path);
            Event[] array = new Event[data.Length];
            for (var i = 0; i < data.Length; i++)
            {
                string line = data[i];
                string[] parts = line.Split(";");

                Event item = new Event(parts);
                array[i] = item;
            }

            return array;
        }
    }
}
